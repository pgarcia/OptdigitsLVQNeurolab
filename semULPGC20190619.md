# Seminario "Técnicas neuronales de procesamiento de información mediante redes neuronales modulares con diferentes tipos de aprendizaje"
Computación Inteligente y Neurociencia computacional

Instituto Universitario de Ciencias y Tecnologías Cibernéticas, Universidad de Las Palmas de Gran Canaria, 19 de junio de 2019

**Patricio García Báez** <pgarcia@ull.es>

Departamento de Ingeniería Informática y de Sistemas,

Universidad de La Laguna


## Índice
* [Introducción a las Redes Neuronales y sus Aplicaciones](http://ddv.stic.ull.es/users/pgarcia/public/sem/intRNAs.pdf)
* [Técnicas neuronales de procesamiento de información mediante redes neuronales modulares con diferentes tipos de aprendizaje](http://ddv.stic.ull.es/users/pgarcia/public/sem/sem_procinfrnmod.pdf)
* [Generalización con LVQ](https://gitlab.com/pgarcia/OptdigitsLVQNeurolab/blob/master/Generalizacion_con_LVQ.ipynb)
* [HUMANN](http://ddv.stic.ull.es/users/pgarcia/public/sem/sem_humann.pdf)

## Referencias
* RNAs
  * [Introducción a los Modelos de Computación Conexionista](https://campusvirtual.ull.es/ocw/course/view.php?id=55)
  * [Neural Network Zoo](http://www.asimovinstitute.org/neural-network-zoo/)
  * [Deep Learning: Una guía de iniciación a Skynet](https://docs.google.com/presentation/d/e/2PACX-1vRe3A1MstHD1xe2dSPobYf3FuyNEL3vQTCQvXONs5qENrex1vRSkEib2ZL8HLB1ploeiOUK1hHoRxBj/embed?start=false&loop=false&delayms=3000#slide=id.p)
* PCA
  * [A layman's introduction to principal component analysis](https://www.youtube.com/watch?v=BfTMmoDFXyE)
* SOMs
  * [Simulation of a Kohonen Self-Organizing Feature Map](https://www.youtube.com/watch?v=zeOtwCAI3Fs)
  * [Self Organizing Map Visualization in 2D and 3D](https://www.youtube.com/watch?v=b3nG4c2NECI)
  * [Growing neural gas network on a waypoint graph](https://www.youtube.com/watch?v=1zyDhQn6p4c)
  * [DemoGNG.js](https://www.demogng.de/)
  * [Gesture and Fingers Recognition by a Neural GAS](https://www.youtube.com/watch?v=b6y1RWhH_fQ)
  * [SOINN: Self-Organizing Incremental Neural Network (recorded Java Applet)](https://www.youtube.com/watch?v=SssDPrbUUqU)
* Ensembles
  * [Ensemble learning](http://www.scholarpedia.org/article/Ensemble_learning)
  * [Ensemble Based Systems in Decision Making](http://ddv.stic.ull.es/users/pgarcia/public/sem/csm06.pdf)
  
  
